import axios from 'axios';
import fs from 'fs';
import download from 'download';
import path from 'path';
const  base64 = require('base-64');

export default function attach(app) {
    app.post('/api/adds', (req, res, next) => {
        let data = req.body;
        let order = [];
        console.log(data)
        let generated  = ``;
        console.log()
        if(Array.isArray(data)){
            data.forEach((element, index) => {
                generated += `Ad ${index + 1} -\n \nAd Format: \n${element.format} \n \nTitle: \n${element.title} \n \nImage Filename: \n${element.link} \n \nHeadline: \n${element.headline} \n \nBody Text: \n${element.body} \n \nDescription: \n${element.description} \n \n`;
            })
        } else {
            generated += `Ad - 1 \n \nAd Format: \n${data.format || ""} \n \nTitle: \n${data.title} \n \nImage Filename: \n${data.link} \n \nHeadline: \n${data.headline} \n \nBody Text: \n${data.body} \n \nDescription: \n${data.description} \n \n`;
        }
        
        res.send(generated)
    });
}