import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import path from 'path';
import session from 'express-session';

const app = express();

app.use(compression());

app.use('/', express.static('public'));

app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
app.use(bodyParser.json({ limit: '50mb' }));

import attackAdds from './externals/adds-api';
attackAdds(app);

app.all('/*', function(req, res) {
    res.sendfile('../public/index.html');
});

const port = 1234;
app.listen(port, () => {
    console.log("App now listening on port " + port);
});