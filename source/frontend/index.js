import React from 'react';
import ReactDOM from 'react-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import Store from './Store';
import { Provider } from 'react-redux';
import {Router} from 'react-router';

import Website from './Layout';

const history = createBrowserHistory();

ReactDOM.render((
    <Provider store={Store}>
        <Router history={history}>
            <Website />
        </Router>
    </Provider>
), document.getElementById('app')); 