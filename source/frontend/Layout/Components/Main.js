import React from 'react';
import { Container, Divider, Card, Input, Button, Form, TextArea, Label, Dropdown, Header, Icon, Confirm } from 'semantic-ui-react'
import axios from 'axios';
const selectOptions = [{ key: 'Short', text: 'Short', value: 'Short', content: 'Short' }, { key: 'Medium', text: 'Medium', value: 'Medium', content: 'Medium' }, { key: 'Long', text: 'Long', value: 'Long', content: 'Long' }]
const formatOptions = [{ key: 'Video-Ad', text: 'Video-Ad', value: 'Video-Ad', content: 'Video-Ad' }, { key: 'ImageAd', text: 'ImageAd', value: 'ImageAd', content: 'ImageAd' }]
class Main extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            length: 0,
            adds: [],
            file_ready: false,
            format: formatOptions[0].value,
            title: "",
            description: "",
            headline: "",
            link: "",
            body: "",
            open: false
        };
    }
    open(){ 
        this.setState({ open: true })
    }
    close(){
        this.setState({ open: false })
    }
    handleInputchange(field, e) {
        let state = this.state;
        state[field] = e.target.value;
        this.setState(state);
    }

    formatChange(field, e) {
        let state = this.state;
        let value = e.target.innerText;
        state[field] = value;
        this.setState(state);
    }

    clearState() {
        let state = this.state;
        state.title = "";
        state.description = "";
        state.headline = "";
        state.link = "";
        state.body = "";
        this.setState(state)
    }
    clearBody() {
        let state = this.state;
        state.body = "";
        this.setState(state)

    }

    fullReset() {
        let state = this.state;
        state.length = 0;
        state.adds = [];
        state.file_ready = false;
        state.format = formatOptions[0].value;
        state.title = "";
        state.description = "";
        state.headline = "";
        state.link = "";
        state.body = "";
        state.open = false;
        this.setState(state)
    }
    
    nextAdd() {
        let state = this.state;
        state.adds.push({
            title: state.title,
            description: state.description,
            headline: state.headline,
            link: state.link,
            body: state.body,
            format: state.format,
            length: selectOptions[state.length].value
        });
        this.clearState();
        this.setState(state);
    }

    exportAdds() {
        let that = this;
        if (this.state.adds.length > 0) {
            let ads = this.state.adds;
            if (this.state.title.length > 0 &&
                this.state.description.length > 0 &&
                this.state.headline.length > 0 &&
                this.state.link.length > 0 &&
                this.state.body.length > 0 &&
                this.state.format.length > 0
            ) {
                ads.push({
                    title: this.state.title,
                    description: this.state.description,
                    headline: this.state.headline,
                    link: this.state.link,
                    body: this.state.body,
                    format: this.state.format,
                    length: selectOptions[this.state.length].value
                })
            }
            axios.post('/api/adds', ads)
                .then(function (response) {
                    if (response.data.length > 0) {
                        that.setState({ file_ready: true, generated_text: response.data })
                    }
                })
                .catch(function (error) {
                });
        } else {
            axios.post('/api/adds', {
                title: this.state.title,
                description: this.state.description,
                headline: this.state.headline,
                link: this.state.link,
                body: this.state.body,
                format: this.state.format,
                length: selectOptions[this.state.length].value
            }).then(function (response) {
                if (response.data.length > 0) {
                    that.setState({ file_ready: true, generated_text: response.data })
                    data = response.data
                }
            }).catch(function (error) {

            });
        }
    }

    download() {
        axios.get('/download').then((res) => {
        }).catch((err) => {
        })
    }

    unload() {
        this.fullReset();
    }

    lengthChange(field, e) {
        let state = this.state;
        let value = e.target.innerText;
        let holder;
        switch (value) {
            case 'Short':
                holder = 0
                break;
            case 'Medium':
                holder = 1
                break;
            case 'Long':
                holder = 2
                break;
            default:
                holder = 3
        }
        state[field] = holder;
        this.clearBody();
        this.setState(state);
    }

    render() {
        let now = new Date();
        let lengths = [160, 350, 9999];

        return (
            <div>
                <Container>
                    <Card size="small" fluid style={{ marginTop: '40px' }} color='red'>
                        <Card.Content>
                            <Card.Header>Create Advertisement </Card.Header>
                            <Divider />

                            <Label>Title</Label>
                            <Input fluid focus placeholder='Title' style={{ marginBottom: '20px' }} onChange={this.handleInputchange.bind(this, 'title')} value={this.state.title || ""} />

                            <Label>Format</Label><br></br>
                            Format Set to : {' '}
                            <Dropdown
                                onChange={this.formatChange.bind(this, 'format')}
                                color="blue"
                                inline
                                header='Select Ad Format'
                                options={formatOptions}
                                defaultValue={formatOptions[0].value}
                            />
                            <Divider />

                            <Label>Image/Video Link</Label>
                            <Input fluid focus placeholder='Image/Video Link' style={{ marginBottom: '20px' }} onChange={this.handleInputchange.bind(this, 'link')} value={this.state.link || ""} />
                            <Form>
                                <Label>Advertisement Body</Label>
                                Input Sizes set to : {' '}
                                <Dropdown
                                    onChange={this.lengthChange.bind(this, 'length')}
                                    color="blue"
                                    inline
                                    header='Select Input Length'
                                    options={selectOptions}
                                    defaultValue={selectOptions[0].value}
                                />
                                <TextArea placeholder='Body Goes Here ...' style={{ marginBottom: '20px' }} maxLength={lengths[this.state.length]} onChange={this.handleInputchange.bind(this, 'body')} value={this.state.body || ""} />
                            </Form>
                            <Label>Headline</Label>
                            <Input fluid focus placeholder='Headline' style={{ marginBottom: '20px' }} maxLength={50} onChange={this.handleInputchange.bind(this, 'headline')} value={this.state.headline || ""} />
                            <Label>Link Description</Label>
                            <Input fluid focus placeholder='Link Description' style={{ marginBottom: '20px' }} onChange={this.handleInputchange.bind(this, 'description')} value={this.state.description || ""} />
                            <Divider />
                            <Label size="large" color="red">
                                {this.state.adds.length}
                            </Label>


                            {this.state.file_ready && <a download={"Export-" + now.getTime() + ".txt"} onClick={this.unload.bind(this)} href={"data:text/plain;charset=utf-8," + this.state.generated_text}>
                                <Button color="green" floated="right" onClick={this.download.bind(this)}> <Icon size="large" name='download' /> Download </Button>
                            </a>
                                ||
                                <Button.Group floated="right">
                                    

                                    <div>
                                        <Button color="orange" onClick={this.open.bind(this)} >Export</Button>
                                        <Confirm size='small' header='Exporting will reset all the fields' open={this.state.open} onCancel={this.close.bind(this)} onConfirm={this.exportAdds.bind(this)} />
                                    </div>
                                    
                                    <Button.Or />
                                    <Button color="blue" onClick={this.nextAdd.bind(this)}>Next Add</Button>
                                </Button.Group>}
                        </Card.Content>
                    </Card>
                </Container>
            </div>
        );
    }
}

export default Main;