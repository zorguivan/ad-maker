import React from 'react';
import { Route, Switch } from 'react-router';
import Main from './Components/Main'

class DefaultLayout extends React.Component {
    render() {
        return (
            <Switch>
                <Route exact path="/" render={() => {
                    return <Main/>
                }} />
            </Switch>
        );
    }
}

export default DefaultLayout;