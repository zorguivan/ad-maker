var webpack = require('webpack');
var path = require('path');
var fs = require('fs');
var nodeExternals = require('webpack-node-externals');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

const ExtractCSS = new ExtractTextPlugin({ // define where to save the file
    filename: 'app.bundle.css',
    allChunks: true,
});

module.exports = [
    {
        context: __dirname + "/source/",
        target: 'node',
        entry: './backend/index.js',
        externals: nodeExternals(),
        output: {
            path: __dirname,
            filename: './build/backend.js',
            libraryTarget: "commonjs2"
        },
        resolve: {
            extensions: ['.js', '.jsx', '.json'],
            modules: [
                path.resolve('./source')
            ]
        },

        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['react', 'es2015', 'stage-0'],
                            plugins: [
                                "transform-es2015-destructuring",
                                "transform-object-rest-spread",
                                "transform-decorators-legacy"
                            ]
                        }
                    },
                },
                {
                    test: /\.scss$/,
                    use: [{
                        loader: "style-loader" // creates style nodes from JS strings
                    }, {
                        loader: "css-loader", // translates CSS into CommonJS,
                        options: { minimize: true }
                    }, {
                        loader: "sass-loader" // compiles Less to CSS
                    }]
                },
            ]
        }
    },
    {
        context: __dirname + "/source/",
        entry: {
            app: "./frontend/index.js",
        },
        output: {
            filename: "[name].js",
            path: __dirname + "/public"
        },
        resolve: {
            extensions: ['.js', '.jsx', '.json', '.less'],
            modules: [
                path.resolve('./source'),
                path.resolve('./node_modules')
            ]
        },
        devtool: 'eval-source-map',
        plugins: [
            ExtractCSS
        ],

        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['react', 'es2015', 'stage-0'],
                            plugins: [
                                "transform-es2015-destructuring",
                                "transform-object-rest-spread",
                                "transform-decorators-legacy"
                            ]
                        },
                    },
                },
                { // regular css files
                    test: /\.css$/,
                    use: ExtractCSS.extract(['css-loader?importLoaders=1']),
                },
                { // sass / scss loader for webpack
                    test: /\.(sass|scss)$/,
                    use: ExtractCSS.extract(['css-loader', 'sass-loader'])
                },
                { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&minetype=application/font-woff" },
                { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },
                { test: /\.png$/, loader: "url-loader?limit=10000&mimetype=image/png" }
            ]
        }
    }
]